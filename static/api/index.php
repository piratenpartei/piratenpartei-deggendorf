<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Factory\AppFactory;

require __DIR__ . '/../../vendor/autoload.php';

class JsonBodyParserMiddleware implements MiddlewareInterface
{
  public function process(Request $request, RequestHandler $handler): Response
  {
    $contentType = $request->getHeaderLine('Content-Type');

    if (strstr($contentType, 'application/json')) {
      $contents = json_decode(file_get_contents('php://input'), true);
      if (json_last_error() === JSON_ERROR_NONE) {
        $request = $request->withParsedBody($contents);
      }
    }

    return $handler->handle($request);
  }
}

$app = AppFactory::create();
$app->setBasePath("/api");

$app->addMiddleware(new JsonBodyParserMiddleware());

$customErrorHandler = function (
  Request $request,
  Throwable $exception,
  bool $displayErrorDetails,
  bool $logErrors,
  bool $logErrorDetails
) use ($app) {
  $response = $app->getResponseFactory()->createResponse()->withStatus(404);
  $response->getBody()->write(
    json_encode([
      'success' => false,
      'error' => $exception->getMessage()
    ], JSON_UNESCAPED_UNICODE)
  );
  return $response;
};
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler($customErrorHandler);

$app->post('/contact-submit', function (Request $request, Response $response, $args) {
  $contact = $request->getParsedBody();
  if (!array_key_exists("name", $contact)
    || !array_key_exists("name2", $contact)
    || !array_key_exists("address1", $contact)
    || !array_key_exists("address2", $contact)
    || !array_key_exists("amount", $contact)
    || !array_key_exists("message", $contact)) {
    $errorResponse = $response->withStatus(400);
    $errorResponse->getBody()->write(
      json_encode([
        'success' => false,
        'error' => "invalid request body"
      ], JSON_UNESCAPED_UNICODE)
    );
    return $errorResponse;
  }

  if (strlen($contact['name2']) > 0) {
    // Honeypot
    $errorResponse = $response->withStatus(400);
    $errorResponse->getBody()->write(
      json_encode([
        'success' => false,
        'error' => "invalid name2"
      ], JSON_UNESCAPED_UNICODE)
    );
    return $errorResponse;
  }

  if (strlen($contact['message']) > 5000) {
    $errorResponse = $response->withStatus(400);
    $errorResponse->getBody()->write(
      json_encode([
        'success' => false,
        'error' => "invalid message"
      ], JSON_UNESCAPED_UNICODE)
    );
    return $errorResponse;
  }

  $emailSubject = "Bestellung UU-Formulare";
  $emailBody = "<!DOCTYPE html>
<html lang=\"de\">
<head>
<meta charset=\"UTF-8\">
<title>" . $emailSubject . "</title>
</head>
<body style=\"padding: 10px; font-family: sans-serif;\">
<h1 style=\"font-size: 1.5em;\">" . $emailSubject . "</h1>
<p>
  <strong style=\"font-weight: bold; font-size: 0.8em;\">Name und Anschrift:</strong>
</p>
<pre style=\"display: block; border: 1px dotted #ccc; padding: 5px;\"><code>" . htmlentities($contact['name']) . "<br/>"
    . htmlentities($contact['address1']) . "<br/>"
    . htmlentities($contact['address2']) . "<br/>"
    . "</code></pre>
<p>
  <strong style=\"font-weight: bold; font-size: 0.8em;\">Anzahl UU-Formulare:</strong>
</p>
<pre style=\"display: block; border: 1px dotted #ccc; padding: 5px;\"><code>" . htmlentities($contact['amount']) . "</code></pre>
<p>
  <strong style=\"font-weight: bold; font-size: 0.8em;\">Nachricht:</strong>
</p>
<pre style=\"display: block; border: 1px dotted #ccc; padding: 5px;\"><code>" . nl2br(htmlentities($contact['message'])) . "</code></pre>
</body>
</html>";

  $emailHeader = array();
  $emailHeader[] = 'MIME-Version: 1.0';
  $emailHeader[] = 'Content-type: text/html; charset=utf-8';
  $emailHeader[] = 'Content-Transfer-Encoding: quoted-printable';
  $emailHeader[] = "From: info@piratenpartei-deggendorf.de";
  $emailHeader[] = "X-Mailer: PHP/" . phpversion();
  mail(
    "info@piratenpartei-deggendorf.de",
    "=?utf-8?b?" . base64_encode($emailSubject) . "?=",
    $emailBody,
    implode("\r\n", $emailHeader)
  );

  $response->getBody()->write(
    json_encode(['success' => true], JSON_UNESCAPED_UNICODE)
  );
  return $response;
});

$app->run();
