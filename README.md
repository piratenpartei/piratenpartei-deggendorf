# Piratenpartei Deggendorf Website

## Build Setup

```bash
# install dependencies
$ yarn install

# DEV: serve with hot reload at localhost:3000
$ yarn dev

# DEV: start php api
$ php -S localhost:8080 -t static/api static/api/index.php

# PROD: build for production
$ yarn generate

# DEV: launch server with generated dist folder
$ yarn start
```
