import colors from './assets/colors'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Server
  server: {
    host: '0.0.0.0'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Piratenpartei Deggendorf',
    title: 'Piratenpartei Deggendorf',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }, {
      hid: 'description',
      name: 'description',
      content: 'Direktkandidat der Piratenpartei für den Wahlkreis Deggendorf: Josef Reichardt' +
        ' » Hilf uns jetzt mit deiner Unterstützungsunterschrift'
    }, {
      name: 'msapplication-TileColor',
      content: colors.secondary
    }, {
      name: 'msapplication-TileImage',
      content: '/favicon/favicon-144.png'
    }, {
      property: 'og:site_name',
      content: 'Piratenpartei Deggendorf'
    }],
    link: [{
      rel: 'shortcut icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }, {
      rel: 'shortcut icon',
      type: 'image/png',
      href: '/favicon.png'
    }, {
      rel: 'icon',
      type: 'image/png',
      href: '/favicon/favicon-32.png',
      sizes: '32x32'
    }, {
      rel: 'icon',
      type: 'image/png',
      href: '/favicon/favicon-48.png',
      sizes: '48x48'
    }, {
      rel: 'icon',
      type: 'image/png',
      href: '/favicon/favicon-96.png',
      sizes: '96x96'
    }, {
      rel: 'icon',
      type: 'image/png',
      href: '/favicon/favicon-128.png',
      sizes: '128x128'
    }, {
      rel: 'icon',
      type: 'image/png',
      href: '/favicon/favicon-256.png',
      sizes: '256x256'
    }, {
      rel: 'apple-touch-icon',
      href: '/favicon/favicon-180.png',
      sizes: '180x180'
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxt/http',
    ...(process.env.NODE_ENV === 'development' ? ['@nuxtjs/proxy'] : []),
    // @formatter:off
    ...(process.env.MATOMO_SITE_ID
      ? [['nuxt-matomo', {
          matomoUrl: 'https://www.piratenpartei-deggendorf.de/matomo/',
          siteId: process.env.MATOMO_SITE_ID,
          cookies: false,
          consentRequired: false,
          doNotTrack: true,
          debug: process.env.MATOMO_DEBUG === 'true'
        }]]
      : []),
    // @formatter:on
    '@nuxtjs/sitemap'
  ],

  // $http: https://http.nuxtjs.org/
  http: {
    browserBaseURL: process.env.BASE_URL || 'http://localhost:3000'
  },

  // @nuxtjs/proxy: https://github.com/nuxt-community/proxy-module
  proxy: {
    ...(process.env.NODE_ENV === 'development'
      ? { '/api/contact-submit': 'http://localhost:8080' }
      : {})
  },

  // @nuxtjs/sitemap: https://github.com/nuxt-community/sitemap-module
  sitemap: {
    hostname: process.env.BASE_URL || 'http://localhost:3000'
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    defaultAssets: false,
    icons: {
      iconfont: 'mdi'
    },
    theme: {
      themes: {
        light: colors
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
